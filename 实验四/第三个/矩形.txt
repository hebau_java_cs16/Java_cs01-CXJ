class juxing extends pingmian {

	double a, b;

	juxing(double a, double b) {
		this.a = a;
		this.b = b;
	}

	public double getArea() {
		return (a*b);
	}

	public double getC() {
		return (2*a+2*b);
	}

}
