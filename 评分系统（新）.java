import java.util.Arrays;
import java.util.Scanner;
public class Pingfen {
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int i,j,sum=0;
		float ave;
		int score[][] = new int [5][10];
		for(i=0;i<5;i++){
			for(j=0;j<10;j++){
				if(j==0){
					System.out.println("请输入选手成绩：");
				}
				score[i][j] = input.nextInt();
			}
			Arrays.sort(score[i]);
		}
		for(i=0;i<5;i++){
			System.out.println("第"+(i+1)+"选手成绩为：");
			for(j=8;j>0;j--){
				sum+=score[i][j];
				System.out.print(score[i][j]+" ");
				if(j==1)
				System.out.print('\n');	
			}
			ave=(float)sum/8;
			System.out.println("该选手最低成绩为"+score[i][0]);
			System.out.println("该选手最高成绩为"+score[i][9]);
			System.out.println("该选手平均成绩为"+ave);
			sum=0;
		}
	}
}
