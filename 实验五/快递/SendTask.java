
public class SendTask {
		private String no;
		private String weight;
		public void sendBefore(){
			System.out.println("货物重量："+weight+"kg");
			System.out.println("订单已发货");
			System.out.println("快递单号"+no);
		}
		public void send(Transportation t,GPS tool){
			t.transport();
			tool.showCoordinate();
		}
		public void sendAfter(Transportation t){
			t.transport();
		}
}
