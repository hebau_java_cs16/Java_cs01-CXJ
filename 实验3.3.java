
import java.util.Scanner;

public class TestJava3_5 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String str2 = "txt";
		String str3 = "jpg";
		String str4 = "psd";
		System.out.println("请输入一串字符串:");
		String str = input.next();
		String a[] = str.split(",");
		int b = 0, c = 0, d = 0, e = 0;
		for (int i = 0; i < a.length; i++) {
			int q = a[i].indexOf(".", e);
			e = q;
			String str1 = a[i].substring(q + 1);
			if (str2.equals(str1)) {
				b++;
			}
			if (str3.equals(str1)) {
				c++;
			}
			if (str4.equals(str1)) {
				d++;
			}
		}
		System.out.println("文件个数:" + a.length);
		if (b != 0) {
			System.out.println("txt文件有" + b + "个");
		}
		if (c != 0) {
			System.out.println("jpg文件有" + c + "个");
		}
		if (d != 0) {
			System.out.println("psd文件有" + d + "个");
		}
		System.out.println("文件首字母大写：");
		for (int i = 0; i < a.length; i++) {
			String str1 = a[i].substring(0, 1);
			System.out.println(str1.toUpperCase());
		}
	}
}