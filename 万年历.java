import java.util.Scanner;


public class NewWannianli {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in=new Scanner(System.in);
		@SuppressWarnings("unused")
		int year,mouth,i;
		System.out.print("请输入一个年份：");
		year = in.nextInt();
		isLeap(year);
		System.out.print("请输入一个年份和月份：");
		year = in.nextInt();
		int month = in.nextInt();
		days(year,month);
		System.out.println(year+"年"+month+"月有"+days(year,month)+"天");
		System.out.println(year+"年"+month+"月距离1900年1月1日有"+totalDays(year,month)+"天");
		printCalender(year,month);
	}
	//判断闰年的方法
	public static boolean isLeap(int year){
		if(year%4==0&&year%100!=0||year%400==0){
			System.out.println(year+"是闰年");
			return true;
		}
		else{
			System.out.println(year+"是平年");
			return false;
		}
	}
	//判断某年某月有多少天的方法
	public static int days(int year, int month){
		int day=0;
		switch(month){
			case 1:  
			case 3:  
			case 5:  
			case 7:  
			case 8:  
			case 10:  
			case 12:  
				day = 31;  
				break;  
			case 4:  
			case 6:  
			case 9:  
			case 11:  
				day = 30;  
            break; 
			case 2:
				if(year%4==0&&year%100!=0||year%400==0){
					day = 29;
				}
				else{
					day = 28;
				}
		}
		return day;
	}
	
	
	//计算某年某月之前距离1900年1月1日的总天数的方法
	public static int totalDays(int year, int month){
		int i,j,sum=0,sum1=0,sum2=0;
		for(i=1900;i<year;i++){
			if(i%4==0&&i%100!=0||i%400==0){
				sum1+=366;
			}
			else{
				sum1+=365;
			}
		}
		for(j=1;j<month;j++){
			if(j==1||j==3||j==5||j==7||j==8||j==10||j==12)
				sum2+= 31;  
			else if(j==4||j==6||j==9||j==11)
				sum2+= 30;  
			else{
				if(year%4==0&&year%100!=0||year%400==0){
					sum2+= 29;
				}
				else{
					sum2+= 28;
				}
			}
		}
        sum = sum1 + sum2;
        return sum;
	}
	
	
	
	//输出某年某月日历的方法的方法
	public static void printCalender(int year, int month){	
		int totalDays = totalDays(year, month);// 月之前
        int MonthDays = days(year,month);// 月的天数
        int sum3 = MonthDays;
        System.out.println("星期一 \t星期二\t星期三\t星期四\t星期五\t星期六\t星期日");
        int day = totalDays % 7;// System.out.print(""+a);
        if (day==7)
            day=0;
        for (int m=1;m<=day;m++)
            System.out.print("\t");
        int flag = day;
        for (int i=1;i<=sum3;i++) {

            if (flag%7==0 && flag!=0)
                System.out.println("");
            flag++;
            System.out.print(i + "\t");
        }
	}
}